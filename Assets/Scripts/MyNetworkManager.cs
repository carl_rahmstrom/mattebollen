﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class MyNetworkManager : NetworkManager
{
	public static MyNetworkManager instance = null;
	private GameObject ball;
	public MyNetworkDiscovery myNetworkDiscovery;
	private UI ui;

	public void Awake ()
	{
		print ("MyNetworkManager Awake");
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
		System.Type type = typeof(UnityEngine.Networking.NetworkManager);
		var baseMethod = type.GetMethod ("Awake", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
		if (baseMethod != null)
			baseMethod.Invoke (this, null);
	}

	public void Start ()
	{
		myNetworkDiscovery = MyNetworkDiscovery.instance;
		ui = UI.instance;
		//myNetworkDiscovery.Initialize ();
	}

	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId)
	{
		print ("OnServerAddPlayer " + conn + " " + playerControllerId);
		ball = (GameObject)Instantiate (playerPrefab, new Vector3 (0, 8, 0), Quaternion.identity);
		NetworkServer.AddPlayerForConnection (conn, ball, playerControllerId);
	}

	public virtual void OnStartLocalPlayer (NetworkConnection conn)
	{
		print ("MyNetworkManager OnStartLocalPlayer " + conn);
	}

	public override void OnStartHost ()
	{
		print ("OnStartHost");
		if (ui.MultiPlayer) {
			myNetworkDiscovery.Initialize ();
			myNetworkDiscovery.StartAsServer ();
		}
	}

	public override void OnStartClient (NetworkClient client)
	{
		print ("OnStartClient");
		//ui.addPlayer ();
		//myNetworkDiscovery.Initialize ();
		//myNetworkDiscovery.StartAsClient ();
		//	discovery.showGUI = false;
	}

	public override void OnClientConnect (NetworkConnection conn)
	{
		print ("OnClientConnect");
		ui.addPlayer ();
		//myNetworkDiscovery.Initialize ();
		//myNetworkDiscovery.StartAsClient ();
		//	discovery.showGUI = false;
	}
		
	//	public override void OnClientSceneChanged (NetworkConnection conn)
	//	{
	//		print ("OnClientSceneChanged");
	//	}
	
	//	public override void OnStopClient ()
	//	{
	//		print ("OnStopClient");
	//		discovery.StopBroadcast ();
	//		discovery.showGUI = true;
	//	}
}
