﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Jump : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
	public static Jump instance = null;
	private bool jumped = false;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}

	public virtual void OnPointerDown (PointerEventData ped)
	{
		//print ("Jump OnPointerDown");
		jumped = true;
	}

	public virtual void OnPointerUp (PointerEventData ped)
	{
		//print ("Jump OnPointerUp");
	}

	public bool isJumped ()
	{
		bool tmp = jumped;
		jumped = false;
		return tmp;
	}
}
