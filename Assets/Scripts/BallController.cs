﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class BallController : NetworkBehaviour
{
	private CubeSetup cubeSetup;
	private UI ui;
	private Joystick joystick;
	private Jump jump;
	private CameraController cameraController;
	private Ground ground;
	//private NameController nameController;
	[SyncVar] public NetworkInstanceId nameNetId;
	[SyncVar] private string playerName;
	[SyncVar (hook = "OnGameOver")] private bool gameOver = false;
	private bool resetPlayer = false;
	public float speed;
	private Rigidbody ball;
	public TextMesh question;
	public TextMesh nameText;
	[SyncVar (hook = "OnChangeScale")] private float scale;
	private Vector3 originalScale;
	private AudioSource source;
	public AudioClip error;
	public AudioClip braJobb;
	public AudioClip victory;
	public Texture texture1;
	public Texture texture2;
	public Texture texture3;
	[SyncVar] public float groundTimer = 0;
	[SyncVar] public bool masterTimer = false;
	private int winScore = 10;

	public void Awake ()
	{
		DontDestroyOnLoad (gameObject);
		originalScale = transform.localScale; 
	}

	void Start ()
	{
		cubeSetup = CubeSetup.instance;
		ui = UI.instance;
		source = GetComponent<AudioSource> ();
		ball = GetComponent<Rigidbody> ();
		joystick = Joystick.instance;
		jump = Jump.instance;
	
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		scale = 1;

		setLevel (ui.getLevel ());
		if (isLocalPlayer) {
			cameraController = CameraController.instance;
			cameraController.setBall (ball.gameObject);
			ui.setBallController (ball.GetComponent<BallController> ());
			ground = Ground.instance;
			ground.setBallController (ball.GetComponent<BallController> ());
			if (ui.MultiPlayer) {
				CmdSpawnName (ball.GetComponent<NetworkIdentity> ().netId, PlayerPrefs.GetString ("playerName"));
			}
		}
	}

	public void setLevel (int level)
	{
		Material material = this.GetComponent<Renderer> ().material;
		if (level == 0) {
			material.mainTexture = texture1;
		} else if (level == 1) {
			material.mainTexture = texture2;
		} else if (level == 2) {
			material.mainTexture = texture3;
		}
	}

	[Command]
	public void CmdSpawnName (NetworkInstanceId ballNetId, string name)
	{
		print ("CmdSpawnName " + name);
		playerName = name;
		TextMesh nameClone = Instantiate (nameText, new Vector3 (0, 1, 0), transform.rotation) as TextMesh;
		nameClone.GetComponent<NameController> ().NameString = playerName + " " + ui.getScore () + "/" + winScore;
		nameClone.GetComponent<NameController> ().ballNetId = ballNetId;
		NetworkServer.SpawnWithClientAuthority (nameClone.gameObject, base.connectionToClient);
	}

	void FixedUpdate ()
	{
		if (isLocalPlayer) {
			if (ui.ResetPlayer) {
				CmdSetScore (nameNetId, PlayerPrefs.GetString ("playerName") + " " + ui.getScore () + "/" + winScore);
				ui.ResetPlayer = false;
			}
			if (cubeSetup.Running) {
				if (!resetPlayer) {
					resetPlayer = true;
				}
			} else {
				if (resetPlayer) {
					resetPlayer = false;
					CmdSetScale (1f);
					//CmdSetScore (nameNetId, PlayerPrefs.GetString ("playerName"));
				}
			}
			if (transform.position.y < -4) {
				if (ui.MultiPlayer) {
					CmdSetScale (1);
					GetComponent<Transform> ().position = new Vector3 (0, 8, 0);
				} else {
					if (ui.getScore () > PlayerPrefs.GetInt ("highScore" + ui.getLevel ())) {
						PlayerPrefs.SetInt ("highScore" + ui.getLevel (), ui.getScore ());
						if (PlayerPrefs.GetString ("playerName") != "") {
							StartCoroutine (ui.savePlayerServer (false, true));
						}
						ui.updatePlayerInfo ();
						source.PlayOneShot (victory);
						ui.showHighScore (null);
					} else {
						ui.show ();
					}
					CmdSetGameOver ();
				}
			}
			if (!ui.MultiPlayer && !cubeSetup.Running) {
				GetComponent<Transform> ().position = new Vector3 (0, 8, 0);
			} else {
//					float moveHorizontal = Input.acceleration.x != 0 ? Input.acceleration.x : Input.GetAxis ("Horizontal");
//					float moveVertical = Input.acceleration.y != 0 ? Input.acceleration.y : Input.GetAxis ("Vertical");
//					Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
				Vector3 movement = joystick.GetDirection ();
				ball.AddForce (movement * speed);
				if (jump.isJumped () && ui.getJumps () > 0) {
					Vector3 up = new Vector3 (0, 100f, 0);
					ball.AddForce (up);
					ui.decJumps ();
				}
				if (ui.getLevel () == 2 && transform.position.y < 1) {
					Vector3 lava = new Vector3 (0, 0, -0.3f);
					ball.AddForce (lava);
				}
			}
		}

		if (isLocalPlayer) {
			if (isServer) {
				masterTimer = true;
				groundTimer += Time.deltaTime;
			} else {
				BallController[] ballControllers = FindObjectsOfType<BallController> ();
				for (int i = 0; i < ballControllers.Length; i++) {
					if (ballControllers [i].masterTimer) {
						groundTimer = ballControllers [i].groundTimer;
					}
				}
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (isLocalPlayer && cubeSetup.Running) {
			CubeController cubeController = other.gameObject.transform.parent.gameObject.GetComponent<CubeController> ();
			if (!cubeController.answered) {
				CmdSetAnsweredCube (other.gameObject.transform.parent.gameObject);
				if (cubeController.correctAnswer) {
					source.PlayOneShot (braJobb);
					ui.incScore ();
					//print ("OnTriggerEnter " + nameNetId + " " + PlayerPrefs.GetString ("playerName") + ":" + ui.getScore ());
					if (ui.MultiPlayer) {
						CmdSetScore (nameNetId, PlayerPrefs.GetString ("playerName") + " " + ui.getScore () + "/" + winScore);
					}
					CmdSetAnswered (cubeController.questionNetId);
					if (scale > 1) {
						CmdSetScale (scale - 0.1f);
					}
					if (ui.MultiPlayer && ui.getScore () >= winScore) {
						CmdSetGameOver ();
					} 
				} else {
					source.PlayOneShot (error);
					Renderer renderer = other.gameObject.transform.parent.gameObject.GetComponent<Renderer> ();
					renderer.material.SetColor ("_Color", new Color32 (230, 74, 25, 1));
					if (scale < 3) {
						CmdSetScale (scale + 0.2f);
					}
				}
			}
		}
	}

	[Command]
	public void CmdSetGameOver ()
	{
		print ("CmdSetGameOver ");
		if (!gameOver) {
		}
		if (ui.MultiPlayer) {
			cubeSetup.Winner = playerName;
		}
		gameOver = true;
	}

	private void OnGameOver (bool gameOverChange)
	{
		print ("OnGameOver " + gameOverChange);
		#if UNITY_IOS || UNITY_ANDROID
			Handheld.Vibrate ();
		#endif
		cubeSetup.stopCubes ();
		ui.setScore (0);
		ui.resetJumps ();
		if (ui.MultiPlayer) {
			source.PlayOneShot (victory);
			ui.showHighScore (cubeSetup.Winner);
		}
		gameOver = false;
	}

	[Command]
	public void CmdSetScale (float scaleChange)
	{
		//	print ("CmdSetScale " + scaleChange);
		scale = scaleChange;
	}

	private void OnChangeScale (float scaleChange)
	{
		//	print ("OnChangeScale " + scaleChange);
		scale = scaleChange;
		transform.localScale = originalScale * scale;
	}

	[Command]
	public void CmdSetAnswered (NetworkInstanceId questionNetId)
	{
		GameObject questionObject = ClientScene.FindLocalObject (questionNetId);
		CubeController cubeController = questionObject.GetComponent<CubeController> ();
		cubeController.answered = true;
	}

	[Command]
	public void CmdSetScore (NetworkInstanceId nameNetId, string text)
	{
		NameController nameController = ClientScene.FindLocalObject (nameNetId).GetComponent<NameController> ();
		nameController.NameString = text;
	}

	[Command]
	public void CmdSetAnsweredCube (GameObject gameObject)
	{
		CubeController cubeController = gameObject.GetComponent<CubeController> ();
		cubeController.answered = true;
	}

	public float GroundTimer {
		get {
			return groundTimer;
		}
	}
}

