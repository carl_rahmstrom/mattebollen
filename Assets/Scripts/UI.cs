﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	public static UI instance = null;
	private MyNetworkManager myNetworkManager;
	public MyNetworkDiscovery myNetworkDiscovery;
	private CubeSetup cubeSetup;
	private Ground ground;
	private WaterController water;
	private StandsController stands;
	private Particles particles;
	private BallController ballController = null;

	private const int JUMPS = 3;
	private int level = 0;
	public Text levelText;
	public Image levelImage;
	public Image levelImageHighScore;
	public Sprite level1;
	public Sprite level2;
	public Sprite level3;
	private int score;
	private int jumps = JUMPS;
	public Image jump1;
	public Image jump2;
	public Image jump3;
	public Text highScore;
	public Text scoreBoard;
	public Text jumpsText;
	public Text player;
	public GameObject leaderBoardPanel;
	public Text leaderBoard;
	public GameObject registerPanel;
	public GameObject highScorePanel;
	public InputField playerName;
	public InputField playerCity;
	public InputField playerOrg;
	public InputField playerYear;
	public Image noSignal;
	public Image noSignalRegistration;
	public Image spinner;
	public Image spinner1;
	public Image spinner2;
	public Image spinner3;
	public Image spinner4;
	public Image check1;
	public Image check2;
	public Image check3;
	public Image check4;
	public Button playButton;
	public Button changeLevelButton;
	public Button leaderBoardButton;
	public Button registerButton;
	public Button saveButton;
	public Button cityButton;
	public Button orgButton;
	public Button yearButton;
	public Button startHostButton;
	public Button startClientButton;
	public Button serverListButton1;
	public Button serverListButton2;
	public Button serverListButton3;
	public Image joystick;
	public Image jump;
	private AudioSource source;
	//private string serverURL = "http://127.0.0.1:8888/";
	private string serverURL = "https://schoolity-game.appspot.com/";
	private Dictionary<string, string> hosts = new Dictionary<string, string> ();
	private List<Button> serverButtons = new List<Button> ();
	private bool multiPlayer = false;
	private bool host = false;
	private bool resetPlayer = false;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
		Application.targetFrameRate = 30;
	}

	public void Start ()
	{
		myNetworkManager = MyNetworkManager.instance;
		myNetworkDiscovery = MyNetworkDiscovery.instance;
		cubeSetup = CubeSetup.instance;
		ground = Ground.instance;
		water = WaterController.instance;
		stands = StandsController.instance;
		particles = Particles.instance;

		level = PlayerPrefs.GetInt ("level");
		updatePlayerInfo ();
		updateJumps ();
		updateTheme ();
		startClient ();
	}

	public void setBallController (BallController ballController)
	{
		this.ballController = ballController;
	}

	public void startHost ()
	{
		if (PlayerPrefs.GetString ("playerName") == "") {
			register ();
		} else {
			setupHost ();
		}
	}

	public void setupHost ()
	{
		NetworkTransport.Shutdown ();
		NetworkTransport.Init ();
		myNetworkDiscovery.StopBroadcast ();
		multiPlayer = true;
		host = true;
		myNetworkManager.StartHost ();
		hideNetworkUI ();
	}

	public void startClient ()
	{
		NetworkTransport.Shutdown ();
		NetworkTransport.Init ();
		myNetworkDiscovery.Initialize ();
		myNetworkDiscovery.StartAsClient ();
	}

	public void menu ()
	{
		if (cubeSetup != null && cubeSetup.Running) {
			cubeSetup.stopCubes ();
		}
		multiPlayer = false;
		show ();
		showNetworkUI ();
		resetJumps ();
		setScore (0);
		myNetworkDiscovery.StopBroadcast ();
		myNetworkManager.StopHost (); // stops both client and server
		clearServers ();
		hosts.Clear ();
		startClient ();
	}

	public void clearServers ()
	{
		foreach (Button button in serverButtons) {
			if (button != null) {
				Destroy (button.gameObject);
			}
		}
		serverButtons.Clear ();
	}

	public void show ()
	{
		if (multiPlayer) {
			resetPlayer = true;
			if (host) {
				playButton.gameObject.SetActive (true);
			}
		} else {
			playButton.gameObject.SetActive (true);
			showControls (false);
		}
	}

	public void hide ()
	{
		playButton.gameObject.SetActive (false);
		changeLevelButton.gameObject.SetActive (false);
	}

	public void updatePlayerInfo ()
	{
		player.text = PlayerPrefs.GetString ("playerName") + "\n" +
		PlayerPrefs.GetString ("playerCity") + "\n" +
		PlayerPrefs.GetString ("playerOrg") + "\n" +
		PlayerPrefs.GetString ("playerYear");
		levelImage.overrideSprite = levelSprite ();
		highScore.text = "" + PlayerPrefs.GetInt ("highScore" + level);
		scoreBoard.text = "" + 0;
	}

	public Button serverListButton (int level)
	{
		if (level == 0) {
			return serverListButton1;
		} else if (level == 1) {
			return serverListButton2;
		} else if (level == 2) {
			return serverListButton3;
		} else {
			return null;
		}
	}

	public void displayHostButton (string data)
	{
		var items = data.Split (':');
		string value = "";
		if (!hosts.TryGetValue (items [0], out value)) {
			print ("displayServer " + data);
			hosts.Add (items [0], data);
			Button hostButtonClone = Instantiate (serverListButton (Convert.ToInt32 (items [3]))) as Button;
			serverButtons.Add (hostButtonClone);
			hostButtonClone.transform.SetParent (instance.transform, false);
			Vector3 v3 = new Vector3 (0, 100 * (hosts.Count - 1), 0);
			hostButtonClone.GetComponent<RectTransform> ().Translate (v3);
			hostButtonClone.GetComponentInChildren<Text> ().text = items [2];
			hostButtonClone.name = items [0];
			hostButtonClone.onClick.AddListener (() => {
				if (PlayerPrefs.GetString ("playerName") == "") {
					register ();
				}
				else {
					joinServer (hostButtonClone.name);
					clearServers ();
				}
			});
			hostButtonClone.gameObject.SetActive (true);
		}
	}

	public void joinServer (string name)
	{
		multiPlayer = true;
		hideNetworkUI ();
		hide ();
		var items = hosts [name].Split (':');
		myNetworkManager.networkAddress = items [0];
		myNetworkManager.networkPort = Convert.ToInt32 (items [1]);
		setLevel (Convert.ToInt32 (items [3]));
		myNetworkManager.StartClient ();
	}

	public void hideNetworkUI ()
	{
		startHostButton.gameObject.SetActive (false);
	}

	public void showNetworkUI ()
	{
		startHostButton.gameObject.SetActive (true);
	}

	public void addPlayer ()
	{
		score = 0;
		Time.timeScale = 1;
		updateScore ();
		showControls (true);
		ClientScene.AddPlayer (myNetworkManager.client.connection, 0);
	}

	public void play ()
	{
		CubeController.speed = multiPlayer ? 1f : 2f;
		clearServers ();
		showControls (true);
		if (!multiPlayer) {
			if (!myNetworkManager.IsClientConnected ()) {
				setupHost ();
			} 
			multiPlayer = false;

//			NetworkTransport.Shutdown ();
//			NetworkTransport.Init ();
//			//myNetworkDiscovery.StopBroadcast ();
//			host = false;
//			if (!myNetworkManager.IsClientConnected ()) {
//				myNetworkManager.StartHost ();
//			}
//			hideNetworkUI ();
		}
		hide ();
		cubeSetup = CubeSetup.instance; // ios bug workaround
		cubeSetup.startCubes ();
	}

	public void showControls (Boolean show)
	{
		joystick.gameObject.SetActive (show);
		jump.gameObject.SetActive (show);
		leaderBoardButton.gameObject.SetActive (!show);
		registerButton.gameObject.SetActive (!show);
		changeLevelButton.gameObject.SetActive (!show);
	}

	public virtual void OnStartLocalPlayer (NetworkConnection conn)
	{
		print ("UI OnStartLocalPlayer " + conn);
	}

	public void changeLevel ()
	{
		if (cubeSetup != null && cubeSetup.Running) {
			return;
		}
		level += 1;
		if (level >= 3) {
			level = 0;
		}
		setLevel (level);
	}

	public void setLevel (int value)
	{
		print ("UI setLevel " + value);
		level = value;
		PlayerPrefs.SetInt ("level", level);
		updatePlayerInfo ();
		updateTheme ();
	}

	public int getLevel ()
	{
		return level;
	}

	public void updateTheme ()
	{
		if (ballController != null) {
			ballController.setLevel (level);
		}
		ground.setLevel (level);
		if (level == 0) {
			water.gameObject.SetActive (false);
			stands.gameObject.SetActive (true);
			particles.gameObject.SetActive (false);
		} else if (level == 1) {
			water.gameObject.SetActive (true);
			stands.gameObject.SetActive (false);
			particles.gameObject.SetActive (false);
		} else if (level == 2) {
			water.gameObject.SetActive (false);
			stands.gameObject.SetActive (false);
			particles.gameObject.SetActive (true);
		} 
	}

	public Sprite levelSprite ()
	{
		if (level == 0) {
			return level1;
		} else if (level == 1) {
			return level2;
		} else if (level == 2) {
			return level3;
		} else {
			return null;
		}
	}

	public void showHighScore (string text)
	{
		if (highScorePanel.gameObject.activeInHierarchy) {
			return;
		}
		highScorePanel.gameObject.SetActive (true);
		//string highScoreText = text == null ? "Personligt rekord!\nNivå " + levelName () + "\nPoäng " + score : text;
		string highScoreText = text == null ? "" + score : text;
		highScorePanel.GetComponentInChildren<Text> ().text = highScoreText;
	}

	public void updateScore ()
	{
		scoreBoard.text = "" + score;
	}

	public int getScore ()
	{
		return this.score;
	}

	public void setScore (int score)
	{
		this.score = score;
		updateScore ();
	}

	public void incScore ()
	{
		this.score++;
		updateScore ();
		if (score % 5 == 0) {
			incJumps ();
		}
	}

	public void updateJumps ()
	{
		if (this.jumps == 0) {
			jump1.gameObject.SetActive (false);
			jump2.gameObject.SetActive (false);
			jump3.gameObject.SetActive (false);
		} else if (this.jumps == 1) {
			jump1.gameObject.SetActive (true);
			jump2.gameObject.SetActive (false);
			jump3.gameObject.SetActive (false);
		} else if (this.jumps == 2) {
			jump1.gameObject.SetActive (true);
			jump2.gameObject.SetActive (true);
			jump3.gameObject.SetActive (false);
		} else if (this.jumps == 3) {
			jump1.gameObject.SetActive (true);
			jump2.gameObject.SetActive (true);
			jump3.gameObject.SetActive (true);
		}
	}

	public int getJumps ()
	{
		return this.jumps;
	}

	public void setJumps (int jumps)
	{
		this.jumps = jumps;
		updateJumps ();
	}

	public void resetJumps ()
	{
		this.jumps = JUMPS;
		updateJumps ();
	}

	public void incJumps ()
	{
		if (this.jumps <= 2) {
			this.jumps++;
		}
		updateJumps ();
	}

	public void decJumps ()
	{
		this.jumps--;
		updateJumps ();
	}

	public void register ()
	{
		if (cubeSetup != null && cubeSetup.Running) {
			return;
		}
		leaderBoardPanel.gameObject.SetActive (false);
		registerPanel.gameObject.SetActive (true);
		playerName.text = PlayerPrefs.GetString ("playerName");
		playerCity.text = PlayerPrefs.GetString ("playerCity");
		playerOrg.text = PlayerPrefs.GetString ("playerOrg");
		playerYear.text = PlayerPrefs.GetString ("playerYear");
		//saveButton.GetComponentInChildren<Text> ().text = "Spara";
	}

	public void savePlayer ()
	{
		bool saveScore = PlayerPrefs.GetString ("playerName") == "" ? true : false;
		if (playerName.text == "") {
			//saveButton.GetComponentInChildren<Text> ().text = "Vänligen ange ett namn.";
			spinners (false, true);
			//StartCoroutine (Example ());
		} else {
			//saveButton.GetComponentInChildren<Text> ().text = "Sparar...";
			spinners (true, false);
			StartCoroutine (savePlayerServer (true, saveScore));
		}
	}

	public void spinners (bool enable, bool error)
	{
		spinner1.gameObject.SetActive (enable);
		spinner2.gameObject.SetActive (enable);
		spinner3.gameObject.SetActive (enable);
		spinner4.gameObject.SetActive (enable);
		if (error) {
			check1.gameObject.SetActive (false);
			check2.gameObject.SetActive (false);
			check3.gameObject.SetActive (false);
			check4.gameObject.SetActive (false);
			noSignalRegistration.gameObject.SetActive (true);
		} else {
			check1.gameObject.SetActive (!enable);
			check2.gameObject.SetActive (!enable);
			check3.gameObject.SetActive (!enable);
			check4.gameObject.SetActive (!enable);
			noSignalRegistration.gameObject.SetActive (false);
		}
	}

	public IEnumerator savePlayerServer (bool registration, bool saveScore)
	{
		print ("savePlayerServer registration " + registration + " saveScore " + saveScore);
		WWWForm form = new WWWForm ();
		form.AddField ("key", "vi33oensSGsvnisv73");
		string name = registration ? playerName.text : PlayerPrefs.GetString ("playerName");
		string city = registration ? playerCity.text : PlayerPrefs.GetString ("playerCity");
		string org = registration ? playerOrg.text : PlayerPrefs.GetString ("playerOrg");
		string year = registration ? playerYear.text : PlayerPrefs.GetString ("playerYear");
		form.AddField ("playerName", name);
		form.AddField ("playerCity", city);
		form.AddField ("playerOrg", org);
		form.AddField ("playerYear", year);
		int highScore0 = saveScore ? PlayerPrefs.GetInt ("highScore0") : 0;
		int highScore1 = saveScore ? PlayerPrefs.GetInt ("highScore1") : 0;
		int highScore2 = saveScore ? PlayerPrefs.GetInt ("highScore2") : 0;
		form.AddField ("playerScore0", "" + highScore0);
		form.AddField ("playerScore1", "" + highScore1);
		form.AddField ("playerScore2", "" + highScore2);
		WWW post = new WWW (serverURL + "saveplayer", form);
		yield return post;

		if (!string.IsNullOrEmpty (post.error)) {
			Debug.Log ("Save error: " + post.error);
			if (registration) {
				//saveButton.GetComponentInChildren<Text> ().text = "Ett fel uppstod. Vänligen kontrollera internetuppkopplingen.";
				spinners (false, true);
			}
		} else {
			print ("savePlayerServer OK");
			if (registration) {
				spinners (false, false);
				//saveButton.GetComponentInChildren<Text> ().text = "Sparat";
				PlayerPrefs.SetString ("playerName", playerName.text);
				PlayerPrefs.SetString ("playerCity", playerCity.text);
				PlayerPrefs.SetString ("playerOrg", playerOrg.text);
				PlayerPrefs.SetString ("playerYear", playerYear.text);
				JSONNode playerServer = JSON.Parse (post.text);
				for (int i = 0; i <= 2; i++) {
					int scoreServer = playerServer ["score" + i].AsInt;
					PlayerPrefs.SetInt ("highScore" + i, scoreServer);
				}
				updatePlayerInfo ();
			}
		}
	}

	public void loadLeaderBoard ()
	{
		if (cubeSetup != null && cubeSetup.Running) {
			return;
		}
		leaderBoardPanel.gameObject.SetActive (true);
		if (PlayerPrefs.GetString ("playerCity") == "") {
			cityButton.gameObject.SetActive (false);
		}
		if (PlayerPrefs.GetString ("playerOrg") == "") {
			orgButton.gameObject.SetActive (false);
		}
		if (PlayerPrefs.GetString ("playerYear") == "") {
			yearButton.gameObject.SetActive (false);
		}
		
		Text cityText = cityButton.GetComponentInChildren<Text> ();
		if (cityText != null) {
			cityText.text = PlayerPrefs.GetString ("playerCity");
		}
		Text orgText = orgButton.GetComponentInChildren<Text> ();
		if (orgText != null) {
			orgText.text = PlayerPrefs.GetString ("playerOrg");
		}
		Text yearText = yearButton.GetComponentInChildren<Text> ();
		if (yearText != null) {
			yearText.text = PlayerPrefs.GetString ("playerYear");
		}

		//leaderBoardHeader.text = "Topplistan - " + levelName () + "\n";
		levelImageHighScore.overrideSprite = levelSprite ();
		leaderBoard.text = "";
		noSignal.gameObject.SetActive (false);
		spinner.gameObject.SetActive (true);
		StartCoroutine (loadLeaderBoardServer ());
	}

	IEnumerator loadLeaderBoardServer ()
	{
		string city = cityButton.gameObject.activeInHierarchy ? WWW.EscapeURL (PlayerPrefs.GetString ("playerCity")) : null;
		string org = orgButton.gameObject.activeInHierarchy ? WWW.EscapeURL (PlayerPrefs.GetString ("playerOrg")) : null;
		string year = yearButton.gameObject.activeInHierarchy ? WWW.EscapeURL (PlayerPrefs.GetString ("playerYear")) : null;
		string url = serverURL + "leaderboard?city=" + city + "&org=" + org + "&year=" + year + "&level=" + level;
		WWW get = new WWW (url);
		yield return get;
		spinner.gameObject.SetActive (false);
		if (get.error != null) {
			//leaderBoard.text = "Ett fel uppstod. Vänligen kontrollera internetuppkopplingen.";
			noSignal.gameObject.SetActive (true);
		} else {
			JSONNode players = JSON.Parse (get.text);
			string leaders = "";
			string space = "";
			int score = 0;
			int lastScore = 0;
			int pos = 1;
			for (int i = 0; i < 10; i++) {
				if (players [i] ["name"].Value != "") {
					score = players [i] ["score" + level].AsInt;
					if (score != lastScore) {
						pos = i + 1;
						lastScore = score;
					}
					if (i <= 9) {
						space = " ";
					}
					leaders += pos + ". " + space
					+ score + " - "
					+ players [i] ["name"].Value + " "
					+ players [i] ["city"].Value + " "
					+ players [i] ["org"].Value + " "
					+ players [i] ["year"].Value + "\n";
				}
			}
			leaderBoard.text = leaders;
		}
	}

	public bool MultiPlayer {
		get {
			return multiPlayer;
		}
	}

	public bool ResetPlayer {
		get {
			return resetPlayer;
		}
		set {
			resetPlayer = value;
		}
	}
}
