﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public Rigidbody ball;
	public float speed;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.acceleration.x != 0 ? Input.acceleration.x : Input.GetAxis ("Horizontal");
		float moveVertical = Input.acceleration.y != 0 ? Input.acceleration.y : Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		ball.AddForce (movement * speed);
	}
}
