﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public static CameraController instance = null;
	private GameObject ball = null;
	private Vector3 offset;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	void LateUpdate ()
	{
		if (ball != null) {
			transform.position = ball.transform.position + offset;
		}
	}

	public void setBall (GameObject ball)
	{
		this.ball = ball;
		offset = new Vector3 (0, 5, -8);
	}
}