﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class CubeSetup : NetworkBehaviour
{
	public static CubeSetup instance = null;
	private UI ui;

	public Rigidbody cube;
	public Rigidbody cubeCorrect;
	private float startZ = 1000;
	private int nrCubes = 10;
	public TextMesh question;
	private float timer = 12;
	[SyncVar] private bool running = false;
	[SyncVar] private string winner;
	private List<Rigidbody> cubes = new List<Rigidbody> ();
	private List<TextMesh> questions = new List<TextMesh> ();

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	void Start ()
	{
		ui = UI.instance;
	}

	void FixedUpdate ()
	{
		if (!running) {
			return;
		}
		timer += Time.deltaTime;
		if (timer >= 12 / CubeController.speed) {
			createCubes ();
			timer = 0;
		}
	}

	public void startCubes ()
	{
		if (!isServer) {
			return;
		}
		running = true;
	}

	public void stopCubes ()
	{
		if (!isServer) {
			return;
		}
		running = false;
		foreach (Rigidbody cube in cubes) {
			if (cube != null && cube.transform.position.z > 10f) {
				Destroy (cube.gameObject);
			}
		}
		foreach (TextMesh question in questions) {
			if (question != null && question.transform.position.z > 10f) {
				Destroy (question.gameObject);
			}
		}
	}

	public void createCubes ()
	{
		if (!isServer) {
			return;
		}
		CmdCreateCubes ();
	}

	[Command]
	public void CmdCreateCubes ()
	{
		int nr1 = 10;
		int nr2 = 10;
		int answer = 0;
		int arithmetic = 0;
		int minAnswer = 0;
		int maxAnswer = 0;
		string op = "";
		int arithmeticRange = 0;
		if (ui.getLevel () == 0) {
			arithmeticRange = 2;
		} else if (ui.getLevel () == 1) {
			arithmeticRange = 3;
		} else if (ui.getLevel () == 2) {
			arithmeticRange = 4;
		}
		float inc = 0.1f;
		if (CubeController.speed > 4) {
			inc = 0.02f;
		} else if (CubeController.speed > 2) {
			inc = 0.05f;
		}
		if (!ui.MultiPlayer) {
			CubeController.speed = CubeController.speed + inc;
		}
		arithmetic = Mathf.RoundToInt (Random.Range (0, arithmeticRange));
		if (arithmetic == 0) {
			if (ui.getLevel () == 0) {
				while (nr1 + nr2 > 10) {
					nr1 = Mathf.RoundToInt (Random.Range (0, 10));
					nr2 = Mathf.RoundToInt (Random.Range (0, 10));
				}
				minAnswer = 0;
				maxAnswer = 10;
			} else if (ui.getLevel () == 1) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 50));
				nr2 = Mathf.RoundToInt (Random.Range (0, 50));
				minAnswer = 0;
				maxAnswer = 100;
			} else if (ui.getLevel () == 2) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 500));
				nr2 = Mathf.RoundToInt (Random.Range (0, 500));
				minAnswer = 0;
				maxAnswer = 1000;
			}
			answer = nr1 + nr2;
			op = " + ";
		} else if (arithmetic == 1) {
			if (ui.getLevel () == 0) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 10));
				nr2 = Mathf.RoundToInt (Random.Range (0, 10));
				if (nr1 < nr2) {
					int tmp = nr1;
					nr1 = nr2;
					nr2 = tmp;
				}
				minAnswer = 0;
				maxAnswer = 10;
			} else if (ui.getLevel () == 1) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 100));
				nr2 = Mathf.RoundToInt (Random.Range (0, 100));
				if (nr1 < nr2) {
					int tmp = nr1;
					nr1 = nr2;
					nr2 = tmp;
				}
				minAnswer = 0;
				maxAnswer = 100;
			} else if (ui.getLevel () == 2) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 1000));
				nr2 = Mathf.RoundToInt (Random.Range (0, 1000));
				minAnswer = -1000;
				maxAnswer = 1000;
			}
			answer = nr1 - nr2;
			op = " - ";
		} else if (arithmetic == 2) {
			if (ui.getLevel () == 1) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 10));
				nr2 = Mathf.RoundToInt (Random.Range (0, 10));
				answer = nr1 * nr2;
				minAnswer = 0;
				maxAnswer = 100;
			} else if (ui.getLevel () == 2) {
				nr1 = Mathf.RoundToInt (Random.Range (0, 30));
				nr2 = Mathf.RoundToInt (Random.Range (0, 30));
				answer = nr1 * nr2;
				minAnswer = 0;
				maxAnswer = 400;
			}
			op = " * ";
		} else if (arithmetic == 3) {
			nr1 = Mathf.RoundToInt (Random.Range (1, 30));
			nr2 = Mathf.RoundToInt (Random.Range (1, 30));
			int tempAnswer = nr1 * nr2;
			answer = nr1;
			nr1 = tempAnswer;
			minAnswer = 1;
			maxAnswer = 30;
			op = " / ";
		}
		
		TextMesh questionClone = Instantiate (question, new Vector3 (0, 0.5f + 2.1f, startZ), transform.rotation) as TextMesh;
		questions.Add (questionClone);
		CubeController cubeController = questionClone.gameObject.GetComponent<CubeController> ();
		cubeController.nr1 = nr1;
		cubeController.nr2 = nr2;
		cubeController.answer = answer;
		cubeController.op = op;
		questionClone.text = nr1 + op + nr2 + " = ";
		questionClone.gameObject.SetActive (true);
		
		NetworkServer.Spawn (questionClone.gameObject);
		
		int position = Mathf.RoundToInt (Random.Range (0, nrCubes));
		for (int i = 0; i < nrCubes; i++) {
			int random;
			if (i == position) {
				random = answer;
			} else {
				random = Mathf.RoundToInt (Random.Range (minAnswer, maxAnswer + 1));
			}
			Rigidbody cubeClone;
			if (answer == random) {
				cubeClone = Instantiate (cubeCorrect, new Vector3 (i * 2 - 9f, 0.5f + 0.6f, startZ), transform.rotation) as Rigidbody;
				cubes.Add (cubeClone);
				cubeController = cubeClone.gameObject.GetComponent<CubeController> ();
				cubeController.correctAnswer = true;
				//cubeClone.gameObject.GetComponent<BoxCollider> ().enabled = false;
				//cubeClone.gameObject.GetComponentsInChildren<TextMesh> () [0].gameObject.GetComponent<BoxCollider> ().enabled = false;
			} else {
				cubeClone = Instantiate (cube, new Vector3 (i * 2 - 9f, 0.5f + 0.6f, startZ), transform.rotation) as Rigidbody;
				cubes.Add (cubeClone);
				cubeController = cubeClone.gameObject.GetComponent<CubeController> ();
				cubeController.correctAnswer = false;
			}
			cubeController.question = questionClone;
			cubeController.questionNetId = questionClone.GetComponent<NetworkIdentity> ().netId;
			cubeController.nr1 = nr1;
			cubeController.nr2 = nr2;
			cubeController.answer = random;
			cubeController.op = op;
			cubeClone.GetComponentInChildren<TextMesh> ().text = "" + random;
			NetworkServer.Spawn (cubeClone.gameObject);
		}
	}

	public bool Running {
		get {
			return running;
		}
		set {
			running = value;
		}
	}

	public string Winner {
		get {
			print ("Winner get " + winner);
			return winner;
		}
		set {
			print ("Winner set " + value);
			winner = value;
		}
	}
}