﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	public int speed;

	void FixedUpdate ()
	{
		gameObject.transform.Rotate (Vector3.forward * -1 * speed);
	}

	public void reset ()
	{
		speed = 0;
		gameObject.transform.rotation = Quaternion.identity;
	}
}
