﻿using UnityEngine;
using System.Collections;

public class HighScore : MonoBehaviour
{

	void FixedUpdate ()
	{
		float t = 1.4f + Mathf.Sin (Time.realtimeSinceStartup * 2) * 0.05f;
		transform.localScale = new Vector3 (t, t, 1);
	}
}
