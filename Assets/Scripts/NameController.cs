﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NameController : NetworkBehaviour
{
	//public static NameController instance = null;
	private GameObject ball;
	private Vector3 offset;
	[SyncVar]
	private string
		nameString;
	[SyncVar]
	public NetworkInstanceId
		ballNetId;

	void Start ()
	{
		TextMesh nameText = GetComponent<TextMesh> ();
		nameText.text = nameString + ":0";
		ball = ClientScene.FindLocalObject (ballNetId);
		ball.GetComponent<BallController> ().nameNetId = GetComponent<NetworkIdentity> ().netId;
		offset = new Vector3 (0, 1, 0);
	}

	void Update ()
	{
		TextMesh nameText = GetComponent<TextMesh> ();
		nameText.text = nameString;
	}

	void LateUpdate ()
	{
		if (ball != null) {
			transform.position = ball.transform.position + offset;
		}
	}

	public string NameString {
		get {
			return nameString;
		}
		set {
			nameString = value;
		}
	}
}