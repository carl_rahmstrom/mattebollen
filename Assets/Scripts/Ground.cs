﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour
{
	public static Ground instance = null;
	private BallController ballController = null;
	private UI ui;
	public Texture texture1;
	public Texture texture2;
	//	public Texture texture3;
	public GameObject lava;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	void Start ()
	{
		ui = UI.instance;
	}

	public void setBallController (BallController ballController)
	{
		this.ballController = ballController;
	}

	void FixedUpdate ()
	{
		float t = 0;
		if (ui.getLevel () == 1) {
			if (ballController == null) {
				t = Time.realtimeSinceStartup / 2;
			} else {
				t = ballController.GroundTimer / 2;
			}
			transform.rotation = Quaternion.Euler (Mathf.Sin (t), Mathf.Sin (t * 2), Mathf.Sin (t * 3));
		}
	}

	public void setLevel (int level)
	{
		Material material = this.GetComponent<Renderer> ().material;
		if (level == 0) {
			material.mainTexture = texture1;
			material.mainTextureScale = new Vector2 (1, 1);
			material.mainTextureOffset = new Vector2 (1, 1);
			transform.rotation = Quaternion.Euler (0, 90, 0);
			lava.SetActive (false);
		} else if (level == 1) {
			material.mainTexture = texture2;
			material.mainTextureScale = new Vector2 (3, 0.5f);
			material.mainTextureOffset = new Vector2 (0.9f, 1);
			transform.rotation = Quaternion.Euler (0, 0, 0);
			lava.SetActive (false);
		} else if (level == 2) {
//			material.mainTexture = texture3;
			material.mainTextureScale = new Vector2 (1, 1);
			material.mainTextureOffset = new Vector2 (1, 1);
			transform.rotation = Quaternion.Euler (0, 180, 0);
			lava.SetActive (true);
		}
	}
}
