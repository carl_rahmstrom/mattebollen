﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CubeController : NetworkBehaviour
{
	public TextMesh question;
	public static float speed;
	private Rigidbody cube;
	[SyncVar] public int nr1;
	[SyncVar] public int nr2;
	[SyncVar] public int answer;
	[SyncVar] public string op = "";
	[SyncVar] public bool correctAnswer;
	[SyncVar] public bool answered;
	private int size = 25;
	[SyncVar] public NetworkInstanceId questionNetId;
	private float t = 0;
	private float twist = 0;

	void Awake ()
	{
		cube = GetComponent<Rigidbody> ();
	}

	void Start ()
	{
		TextMesh question = GetComponent<TextMesh> ();
		if (question != null) {
			question.text = nr1 + op + nr2 + " = ";
		} else {
			cube.GetComponentInChildren<TextMesh> ().text = "" + answer;
		}
		twist = Random.Range (-100, 100) / (float) 100;
	}

	void FixedUpdate ()
	{
		TextMesh question = GetComponent<TextMesh> ();

		if (question != null && answered) {
			question.text = nr1 + op + nr2 + " = " + answer;
		}
		float cubeSpeed;
		float x = transform.position.x;
		float y = transform.position.y;
		float z = transform.position.z;
		if (transform.position.z > 10f) {
			cubeSpeed = speed * (transform.position.z - 9);
		} else {
			cubeSpeed = speed;
		}
		if (answered && correctAnswer) {
			y += 0.05f * cubeSpeed;
			cubeSpeed = cubeSpeed * -10;
			t += 3 * twist;
			transform.rotation = Quaternion.Euler (transform.rotation.x + t, transform.rotation.y + t, transform.rotation.z + t);
		}

		cube.MovePosition (new Vector3 (x, y, z - 0.025f * cubeSpeed));
		if (z <= -size || z > 1000) {
			NetworkServer.Destroy (gameObject);
		}
	}
}
