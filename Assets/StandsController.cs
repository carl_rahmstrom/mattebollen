﻿using UnityEngine;
using System.Collections;

public class StandsController : MonoBehaviour
{

	public static StandsController instance = null;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}
}
