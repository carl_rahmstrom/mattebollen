﻿using UnityEngine;
using System.Collections;

public class WaterController : MonoBehaviour
{
	public static WaterController instance = null;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}
}
