﻿using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour
{
	public static Particles instance = null;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	void LateUpdate ()
	{
		float t = Time.realtimeSinceStartup / 4;
		int l = 40;
		transform.position = new Vector3 (l * Mathf.Sin (t), 0, l * Mathf.Cos (t));
	}
}
